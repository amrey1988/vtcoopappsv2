import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:http/http.dart' as http;

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});
  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  var client = http.Client();

  void _incrementCounter() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: WebView(
        initialUrl: 'https://vtcoopslinks.biz/K003',
        javascriptMode: JavascriptMode.unrestricted,
      ),
    );
  }

  Future _getVersion() async {
    var url =
        Uri.parse('https://vtcoopslinks.biz/VtCoopApi/json/version?refid=K003');
    var response = await client.get(url);

    if (response.statusCode == 200) {
    } else {}
  }
}
